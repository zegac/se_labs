from django.urls import path
from . import views

app_name = "polls"
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>', views.DetailView.as_view(), name='detail'),
    # POST: /polls/1/upvote,
    path('<int:image_id>/upvote', views.upvote, name='upvote'),
    # POST: /polls/1/downvote
    path('<int:image_id>/downvote', views.downvote, name='downvote'),
    # POST: /polls/1/comment
    path('<int:image_id>/comment', views.comment, name='comment')
]