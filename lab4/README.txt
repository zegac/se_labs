sudo apt update
sudo apt install python3-pip
sudo pip3 install pip --upgrade
sudo pip3 install django
python3 -m django --version

django-admin startproject mysite
python3 manage.py runserver
python3 manage.py startapp polls
python3 manage.py migrate
python3 manage.py makemigrations polls
python3 manage.py sqlmigrate polls 0001
python3 manage.py migrate

python3 manage.py shell

from polls.models import Choice,Question
Question.objects.all()
from django.utils import timezone
q = Question(question_text="Whats new?", pub_date=timezone.now())
q.save()
q.id
q.pub_date
q.question_text
Question.objects.filter(id=1)
Question.objects.get(id=1)

python3 manage.py createsuperuser





